import React from "react";
import Message from "../app/components/Message/Message";

describe("Message in chat", ()=>{
	const wrapper = shallow(<Message />)
	it("should return a div", ()=>{
		expect(wrapper.type()).to.eql("div");
	})
})