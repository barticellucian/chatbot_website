var webpack = require('webpack');
var path = require('path');
var autoprefixer = require('autoprefixer');

const PATHS = {
	source: path.resolve(__dirname, 'app'),
	build: path.resolve(__dirname, 'build')
}

module.exports = {
	devtool: 'source-map',
	entry: [
		'babel-polyfill',
		PATHS.source
	],
	output: {
		path: __dirname+"/build",
		filename: 'bundle.js'
	},
    resolve: {
    	extensions: ['', '.js']
    },
	module: {
		loaders: [
			{
				test: /\.js?$/,
				loader: 'babel',
				include : path.resolve(__dirname,"app"),
				query: {
					presets :['es2015', 'react'],
					env: {
						start: {
							presets: ['react-hmre']
						}
					}
				}
			},
			{
				// from right to left for all the scss files
				// You know what SASS does
				// The css-loader will go through the CSS file and find url() expressions and resolve them. 
				// The style-loader will insert the raw css into a style tag on your page.
				test: /\.scss/,
				loader: 'style!css!postcss-loader!sass'
			},
			{
				test: /\.html/,
				loader: 'html'
			},
			{
				// THEORETICALLY --> images that are 25KB or smaller in size will be converted to a BASE64 string and included in the CSS file where it is defined.
				test: /\.(png|jpg|gif)$/,
			    loader: 'file-loader?name=/assets/images/[name].[ext]'
			},
			{
				test: /\.(eot|woff2|woff|ttf|svg)$/,
			    loader: 'file-loader?name=/assets/fonts/[name].[ext]'
			},
			{
				test: /\.webm$/,
			    loader: 'file-loader?name=/assets/videos/[name].[ext]'
			}
		],
		// preLoaders: [
		// 	{
		// 		test: /\.js$/, // include .js files
  //               exclude: /node_modules/, // exclude any and all files in the node_modules folder
  //               loader: "jshint-loader"
		// 	}
		// ]
	},
	postcss: function(){
		return [autoprefixer];
	},
	jshint: {
		"globals" : {
		    "alert" : false
		},
		"esversion": 6,
		"evil": true, //Invalid on eval()
		"regexdash": true, //Warn on hyphens not being escaped in Regular Expressions
		"browser": true, //if the standard browser globals should be predefined
		"wsh": true, //This option defines globals available when your code is running as a script for the Windows Script host
		"trailing": true, //This option makes it an error to leave a trailing whitespace in your code
		"sub": true, //Allows object['property'] as opposed to object.property
		"curly" : true, //This option requires you to always put curly braces around blocks in loops and conditionals
		"quotmark" : "double", //Defines the type of quote to use in strings
		"undef" : true, //This option prohibits the use of explicitly undeclared variables
		"unused" : true, //This option warns when you define and never use your variables. 
		"newcap" : true, //This option requires you to capitalize names of constructor functions.
		"latedef" : true, //This option prohibits the use of a variable before it was defined
		"camelcase" : true, //This option allows you to force all variable names to use either camelCase style or UPPER_CASE with underscores.
		"maxerr" : 2000 //This sets the maximum number of errors before JSHint terminates, set to a high value in case of inherited code      }
	},
	presets: [
		["es2015", {"modules": false}],
		"stage-2",
		"react"
	]
}