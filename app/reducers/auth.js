import {createReducer} from '../utils';
import {LOGIN, LOGOUT} from '../constants';

const initialState = {
	token: null,
	isAuthenticated: false,
	statusText: null
};

export default createReducer(initialState, {
	[LOGIN]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticated': true,
            'token': payload.token,
            'statusText': 'You have been successfully logged in.'
        });
    },
    [LOGOUT]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticated': false,
            'token': null,
            'statusText': `You have been successfully logged out.`
        });
    }
});