import {combineReducers} from 'redux';
import {routerReducer } from 'react-router-redux';
import auth from './auth';
import main from './main';

export default combineReducers({
	auth,
	main,
	routing: routerReducer
})