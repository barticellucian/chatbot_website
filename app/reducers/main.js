import {createReducer} from '../utils';
import {SHOW_PAGE_ON_MOBILE, VISITED_PAGES, UPDATE_DATA} from '../constants';

const initialState = {
	showPageOnMobile: false,
	visitedPages: {},
    name: "",
    previousPath: null,
    currentPath: null,
    preventReload: false,
    activeMenu: false
};

export default createReducer(initialState, {
	[SHOW_PAGE_ON_MOBILE]: (state, payload) => {
        return Object.assign({}, state, {
            'showPageOnMobile': payload.state
        });
    },
    [VISITED_PAGES]: (state, payload) => {
        return Object.assign({}, state, {
            'visitedPages': payload.visitedPages
        });
    },
    [UPDATE_DATA]: (state, payload) => {
        payload.preventReload = payload.preventReload || false;
        return Object.assign({}, state, payload);
    }
});