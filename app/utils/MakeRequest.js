var createCORSRequest = function(method, url){
  var xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {
    // XHR for Chrome/Firefox/Opera/Safari.
    xhr.open(method, url, true);
  } else if (typeof XDomainRequest != "undefined") {
    // XDomainRequest for IE.
    xhr = new XDomainRequest();
    xhr.open(method, url);
  } else {
    // CORS not supported.
    xhr = null;
  }
  return xhr;
}

var makeRequest = function(opts) {
  return new Promise(function (resolve, reject) {
    // var xhr = new XMLHttpRequest();
    // xhr.open(opts.method, opts.url);
    // 
    var xhr = createCORSRequest(opts.method, opts.url);

    if (!xhr) {
      return reject({statusText:"Cors not supported"});
    }

    xhr.onload = function () {
      if (this.status >= 200 && this.status < 300) {
        resolve(xhr.response);
      } else {
        reject({
          message: JSON.parse(xhr.response).message,
          status: this.status,
          statusText: xhr.statusText
        });
      }
    };
    xhr.onerror = function () {
      reject({
        status: this.status,
        statusText: xhr.statusText
      });
    };
    if (opts.headers) {
      Object.keys(opts.headers).forEach(function (key) {
        xhr.setRequestHeader(key, opts.headers[key]);
      });
    }

    if(typeof opts.params !== "undefined"){
      var params = JSON.stringify(opts.params);
    }

    

    // We'll need to stringify if we've been given an object
    // If we have a string, this is skipped.
    // if (params && typeof params === 'object') {
    //   params = Object.keys(params).map(function (key) {
    //     return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    //   }).join('&');
    // }

    

    xhr.send(params);
  });
};



module.exports = makeRequest;