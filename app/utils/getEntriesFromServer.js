import {SERVER_URL, TOKEN} from "./config";
import MakeRequest from "./MakeRequest";


const getEntriesFromServer = (cat)=>{
    return new Promise((resolve, reject)=>{
         MakeRequest({
            method: 'GET',
            url: `${SERVER_URL}/rest/api/collections/get/${cat}?token=${TOKEN}`,
            headers: {
                "Accept": "application/json"
            }
        })
        .then(function(response){                
            return resolve(JSON.parse(response));
        })
        .catch(function (err) {
            return reject(err);
        });
    })
}

export default getEntriesFromServer;