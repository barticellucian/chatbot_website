import React from 'react';
import { Route, IndexRoute } from 'react-router';
import View from "./containers/View";
import Page from "./containers/Page";
import DynamicPage from "./containers/DynamicPage";

export default (
  <Route path="/" component={View}>
    	<IndexRoute component={Page} />
    	<Route path="about" component={Page} />
    	<Route path="work" component={Page} />
    	<Route path="contact" component={Page} />
    	<Route path="studies/:slug" component={DynamicPage} />
    	<Route path="tools/:slug" component={DynamicPage} />
  </Route>
);
