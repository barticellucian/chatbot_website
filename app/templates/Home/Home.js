import React from "react";
import style from "./style.scss";
import Tiles from "../../components/Tiles";

module.exports = function(){
	return(
		<div className="home page">
			<Tiles page="home"></Tiles>
		</div>
	) ;
};

