import {
	LOGIN,LOGOUT,MESSAGES,SHOW_PAGE_ON_MOBILE,VISITED_PAGES,UPDATE_DATA
} from '../constants';

import store from "../store/index.js";
import { browserHistory } from 'react-router';

export function login(response)
{
	localStorage.setItem("token", response.token);
	return {
		type: LOGIN,
		payload: {
			token: response.token
		}
	}
}

export function logout()
{
	localStorage.removeItem("token");
	return {
		type: LOGOUT,
		payload: {
			// 
		}
	}
}

export function showPageOnMobile(state)
{
	return{
		type: SHOW_PAGE_ON_MOBILE,
		payload: {
			state: state
		}
	}
}

export function firstLand(path, name)
{
	return{
		type: UPDATE_DATA,
		payload: {
			currentPath: path,
			name: name
		}
	}
}
export function navigate(path, subpage)
{
	let previousPath = store.getState().main.currentPath,
		currentPath = subpage ? previousPath : path;
	browserHistory.push(path);

	return{
		type: UPDATE_DATA,
		payload: {
			currentPath: currentPath,
			previousPath: previousPath,
			activeMenu: false
		}
	}
}

export function visitSubPage(category, slug)
{
	let currentVisited = store.getState().main.visitedPages;
	if(!currentVisited.hasOwnProperty(category)) currentVisited[category] = [];
	currentVisited[category].push(slug);

	return{
		type: UPDATE_DATA,
		payload: {
			showPageOnMobile: true,
			visitedPages: currentVisited,
			activeMenu: false
		}
	}
}


export function clearVisitedPages()
{
	return{
		type: UPDATE_DATA,
		payload: {
			visitedPages: {},
			preventReload: true
		}
	}
}

export function toggleMenu(bool)
{
	return{
		type: UPDATE_DATA,
		payload: {
			activeMenu: bool,
			preventReload: true
		}
	}
}