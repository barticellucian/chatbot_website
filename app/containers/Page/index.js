import React from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";
import Tiles from "../../components/Tiles";

class Page extends React.Component{
	render()
	{
		return <div className="page">
				<Tiles page={this.props.location.pathname}></Tiles>
		    </div>;
	}
}

const mapStateToProps = function(state) {
	return {	
		router: state.routing
	}
};

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Page);