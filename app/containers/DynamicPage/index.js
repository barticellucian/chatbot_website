import React from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";
import {SERVER_URL, TOKEN} from "../../utils/config";
import MakeRequest from "../../utils/MakeRequest";
import RelatedContent from "../../components/RelatedContent";
import getEntriesFromServer from "../../utils/getEntriesFromServer";
import Leaders from "../../components/Leaders";
import style from "./style.scss";
import uuid from "../../utils/uuid";
// import { Link, browserHistory } from 'react-router'

class DynamicPage extends React.Component{
	constructor(props){
        super(props);

        this.state = {
            related: [],
            thumb: "",
            serverPath: ""
        };
    }

    componentDidMount() {
        let serverPath = ((url)=>{
            let urlarr = url.split("/");
            urlarr.pop();
            return urlarr.join("/")
        })(SERVER_URL);

        this.state.serverPath = serverPath;
        this.display(this.props); 
    }
    
    componentWillReceiveProps(nextProps){ 
        this.display(nextProps); 
    }

    display(props)
    {   
        let pathArr = props.router.locationBeforeTransitions.pathname.split("/"),
            category = pathArr[1],
            slug = pathArr[2];

        // this.setState({"category": category});
        this.state.category = category;

        getEntriesFromServer(category)
            .then(entries=>this.getContent(entries, slug))
            .then(entriesLeft=>this.setRelatedEntries(entriesLeft))
            .then(()=>{
                this.forceUpdate();
                store.dispatch(actionCreators.visitSubPage(category, this.state.title));
                return true;
            })
            .catch(err=> console.log(err))
    }

    getContent(entries, slug)
    {
        return new Promise((resolve, reject)=>{
            entries.forEach((entry, index, array)=>{
                if(entry.title_slug === slug){
                    this.state.extra = null;
                    Object.assign(this.state, this.parseMedia(array.splice(index, 1)[0]));
                    return true;
                    // return this.setState();
                }
                    
            })
            resolve(entries);
        })
    }

    parseMedia(entry)
    {
        entry.content = entry.content.replace(/src="\/media/g, `src="${SERVER_URL}/media`);
        return entry;
    }

    setRelatedEntries(entries)
    {
        // set the related content at the bottom of the page here
        

        let links = entries.map((item)=>{
            let bgcode = {};
            if(item.hasOwnProperty("thumb")){
                item.thumb = item.thumb.toString().replace("site:cms/", `${this.state.serverPath}/`);
                bgcode = { backgroundImage: `url(${item.thumb})` };
            }
            return <div className="inlineOption" style={bgcode} key={uuid()} onClick={()=>{store.dispatch(actionCreators.navigate(`/${this.state.category}/${item.title_slug}`, true))}}><span>{item.title}</span></div>
        })
        this.state.related = links;
        // this.setState({"related": links});
    }

    addExtraContent(extra)
    {
        let component = null;
        switch(extra){
            case "leaders":
                component = <Leaders></Leaders>;
            break;
        }

        return component;
    }

	render(){
        let extraComponent = this.state.extra ? this.addExtraContent(this.state.extra) : "";
        let bgcode = { backgroundImage: `url(${this.state.thumb.replace("site:cms/", `${this.state.serverPath}/`)})` };
		return (
            <div className="caseStudy" data-id={this.state.title_slug} key={uuid()}>
            	<div className="pageTop" style={bgcode}>
                    <h1><span>{this.state.title}</span></h1>
                </div>
                <div className="caseContent" dangerouslySetInnerHTML={{__html: this.state.content}}></div>
                {extraComponent}
                <RelatedContent related={this.state.related} cat={this.state.category}></RelatedContent>
            </div>
        );
	}
};

// <RelatedContent category={this.state.category}></RelatedContent>
const mapStateToProps = function(state) {
	return {	
		router: state.routing
	}
};

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(DynamicPage);