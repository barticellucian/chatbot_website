import React from "react";
import ReactDOM from "react-dom";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";
import style from "./style.scss";
import Header from "../../components/Header/Header";
import Chat from "../../components/Chat/Chat";

class Page extends React.Component{
	constructor(props){
        super(props);
        this.handleMobileBack = this.handleMobileBack.bind(this);
        this.scrollTimeout = null;
  }

  componentDidMount()
  {
      let path = this.props.location.pathname;
      let clientName = this.getClientName(this.props.location.query);
      store.dispatch(actionCreators.firstLand(path, clientName));
  }
  componentWillReceiveProps(props)
  {
    if(this.scrollTimeout) return;
    this.scrollToTop();
  }

  getClientName(queryString)
  {
    if(queryString.hasOwnProperty("name")){
      return " "+queryString.name.split(" ").map((namePart)=>{
        return namePart.charAt(0).toUpperCase() + namePart.slice(1);
      }).join(" ");
    }
    return "";
  }

  handleMobileBack()
  {
      store.dispatch(actionCreators.showPageOnMobile(false));
  }

  scrollToTop()
  {
        let element = ReactDOM.findDOMNode(this.pageContent);
        // let to = element.scrollHeight;
        let duration = 800;

        var start = element.scrollTop,
        change = 0 - start,
        currentTime = 0,
        increment = 20;

        Math.easeInOutQuad = function (t, b, c, d) {
            t /= d/2;
            if (t < 1) return c/2*t*t + b;
            t--;
            return -c/2 * (t*(t-2) - 1) + b;
        };
        
        const animateScroll = () => {        
            currentTime += increment;
            var val = Math.easeInOutQuad(currentTime, start, change, duration);
            element.scrollTop = val;
            if(currentTime < duration) {
                this.scrollTimeout = setTimeout(animateScroll, increment);
            }else{
                this.scrollTimeout = null;
            }
        };

        animateScroll();
  }

	render(){
		let wrapClass = this.props.main.showPageOnMobile ? "showPageOnMobile" : "";
		return <div id="pageWrap" className={`${wrapClass} ${this.props.location.pathname}`}>
              <Header active={this.props.main.activeMenu}></Header>
              <main id="main">    
                <Chat {...this.props}></Chat>
                <div id="pageContent" ref={(el)=> {this.pageContent = el; }}>
                  <button className="mobileBack" onClick={this.handleMobileBack}>back to chat</button>
                  
                	{this.props.children}
                  <button className="mobileBack bottom" onClick={this.handleMobileBack}>back to chat</button>
                </div>
              </main>
            </div>;
	}
};


const mapStateToProps = function(state) {
	return {	
        main: state.main,
        router: state.routing
	}
};

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Page);