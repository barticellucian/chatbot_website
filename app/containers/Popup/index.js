import React from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";
import {changePage} from "../../actions";
import style from "./style.scss";

export default class Popup extends React.Component{
	constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClose(e){
    	store.dispatch(actionCreators.closePopup());
    }

	handleClick(e){
		switch (this.props.open){
			case "references":
				console.log("open references");
				break;
			case "popup":
				console.log("open popup with target: "+this.props.popupTarget);
				break;
		}
		// store.dispatch(changePage(page));
	}

	render(){
		let className = "popup";
		let Template = require(`../../templates/popups/${this.props.popupTarget}/index.js`);
		if(this.props.popupActive){
			className += " active";
		}
		
		return (
			<div className={className}>
				<div className="top">
					<button className="close" onClick={this.handleClose}></button>
				</div>
				<Template />
			</div>
		)
	}
};

const mapStateToProps = function(state) {
	return {
	    popupActive: state.main.popupActive,
	    popupTarget: state.main.popupTarget
	}
};

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Popup);