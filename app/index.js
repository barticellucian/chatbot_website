import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, Link, hashHistory, browserHistory,useRouterHistory } from "react-router";
import { Provider } from "react-redux";
import * as actionCreators from './actions';
import store from "./store/index.js";
import { createHashHistory } from 'history';

// import { AppContainer } from 'react-hot-loader';
// import {loginUserSuccess} from './actions';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
// import View from "./containers/View/View";
import routes from './routes';

const history = syncHistoryWithStore(browserHistory, store)

// const appHistory = useRouterHistory(createHashHistory)({ queryKey: false })

let token = localStorage.getItem('token');
if (token !== null) {
    store.dispatch(actionCreators.login(token));
}

ReactDOM.render(
	<Provider store={store}>
		<Router routes={routes} history={history} />
	</Provider>,
	document.querySelector(".bodyWrapper")
);