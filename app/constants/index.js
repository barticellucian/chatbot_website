// import {createConstants} from '../utils';
var constants = {
	LOGIN: "LOGIN",
	LOGOUT: "LOGOUT",
	MESSAGES: "MESSAGES",
	SHOW_PAGE_ON_MOBILE: "SHOW_PAGE_ON_MOBILE",
	UPDATE_DATA: "UPDATE_DATA",
	MESSAGE:
		{
			SENDER:{
				EBEE: "ebee",
				USER: "user"
			}
		},
	OPTION_LABEL:{
		"work": "see some work",
		"about": "hear about ebee",
		"contact": "get in touch",
		"surprise": "surprise me"
	},
	VISITED_PAGES: "VISITED_PAGES"
};
module.exports = constants;