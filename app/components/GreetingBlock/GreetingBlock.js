import React from "react";
import Typer from "../Typer";

export default (props)=>{
	let date = new Date();
	let hours = ("0"+date.getHours()).slice(-2);
	let minutes = ("0"+date.getMinutes()).slice(-2);

	let activeClass = props.active ? "active": "";
	return (
		<div className={`greetingBlock ${activeClass}`}>
			<div className="content">
				<div className="date">TODAY @{`${hours}:${minutes}`}</div>
				<Typer>Hello!</Typer>
				
				<p>
					we are ebee, a specialist <br/>
					digital healthcare agency. <br/>
					we make things people want*. <br/>
				</p>
				<p>
					<strong>so, what do you want today?</strong>
				</p>
			</div>
		</div>
	)
} 