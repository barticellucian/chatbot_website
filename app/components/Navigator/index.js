import React from "react";
import ReactDOM from "react-dom";

import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";


export default (props)=>{
    return <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate(`/${props.href}`))}}>{props.children}</div>
}