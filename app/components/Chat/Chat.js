import React, {Component} from "react";
import ReactDOM from "react-dom";
import {SERVER_URL} from "../../utils/config";
// import MakeRequest from "../../utils/MakeRequest";
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";
import style from "./style.scss";

import Message from "../Message/Message";
import Surprise from "../Message/MessageSurprise"; 
import Options from "../Message/MessageOptions"; 
import uuid from "../../utils/uuid.js";
import MessagesController from "../../controllers/Messages";
import GreetingBlock from "../GreetingBlock/GreetingBlock";


export default class Chat extends Component{
	constructor(props){
        super(props);
        this.optionsTimeout = null;
        this.messenger = null;
        this.writing = false;
        this.applyScenario = this.applyScenario.bind(this);
        this.messageController = new MessagesController();
        this.scenario = "";
        this.state = {
            messages: [],
            options: [],
            greetingActive: false
        }

    }

    componentDidMount() {
        this.messageController.init()
            .then((response)=>{
                this.setState({greetingActive: true});
            })
            .catch((err)=>{
                console.log(err);
            })
    }

    componentWillReceiveProps(nextProps)
    {
        if(nextProps.main.preventReload) return;
        if(nextProps.main.previousPath){
            if(nextProps.main.currentPath !== nextProps.main.previousPath){
                return this.applyScenario("navigate", nextProps.main.currentPath, nextProps.main.name, nextProps.main.previousPath, nextProps.main.visitedPages);    
            }
            return false;
        }
        return this.applyScenario("first-land", nextProps.main.currentPath, nextProps.main.name);
    }

    applyScenario(scenario, path = null, clientName="", previousPath="", visitedPages={})
    {
        this.scenario = scenario;
        let messagesSet = this.messageController.getMessageSet(scenario, path, clientName, previousPath, visitedPages);
        this.handleAddMessages(messagesSet);
    }

    handleAddMessages(messagesList)
    {
        this.state.options = [];
        //start adding the messages from the messages array
        this.addMessages(messagesList.messages)
            .then(()=>{
                //start adding the options from the options array
                return this.addOptions(messagesList.options);
            })
            .then(()=>{
                store.dispatch(actionCreators.clearVisitedPages());
            })
            .catch((err)=>{
                console.log(err);
            })
    }

    

    
    addMessages(messages = [])
    {

        //check if there's a message been written at this moment
        if(this.state.messages.length && this.writing)
        {
            //removes any messages being written at this moment
            this.state.messages.pop();
        }
        
        //create an array iterator. we will use it later so we can call .next() on it
		this.messenger = messages.values();

        
		return new Promise((resolve, reject)=>{
			this.showNextMessage()
    			.then(()=>{
    				resolve("done");
    			})
    			.catch((err)=>{
    				console.log(err);
    			});
		})
    	
    }

    showNextMessage()
    {

    	return new Promise((resolve, reject)=>{
            let isFirstEbee = true;
            //this function is also being used as a callback after the current message has ended showing up the message
	    	const next = () => {
                this.messageLoaded();
                //get next value in the iterator from addMessages();
	    		let generatedObj = this.messenger.next().value;

                //resets the flag for a message being written at this moment
                this.writing = false;
                // check if the next value actually exists and resolves the promise if it does not
                
	    		if(!generatedObj){
	    			return resolve("done");
	    		}	
                
                // console.log(3); 
                //if the next value in the iterator exists, set the flag for it being written at this moment
                this.writing = true;
                //add the message component to the chat

                if(typeof generatedObj.message === "string"){
                    this.state.messages.push(<Message key={uuid()} onMessageComputed={next.bind(this)} isFirstEbee={isFirstEbee} sender={generatedObj.sender}>{generatedObj.message}</Message>);    
                }else{
                    if(generatedObj.message.type === "surprise"){
                        this.state.messages.push(<Surprise key={uuid()} onMessageComputed={next.bind(this)} sender={generatedObj.sender}>{generatedObj.message}</Surprise>);
                    }else{
                        this.state.messages.push(<Options key={uuid()} onMessageComputed={next.bind(this)} sender={generatedObj.sender} path={this.props.location.pathname}>{generatedObj.message}</Options>);
                    }
                }

                if(generatedObj.sender === "ebee" && isFirstEbee){
                    isFirstEbee = false;
                }
                
                this.messageLoaded();
                
	    	}

	    	next();
			
    	})
    }

 
    messageLoaded(e)
    {
        let to = this.scenario === "first-land" ? 1850 : 500;
        setTimeout(()=>{
            this.forceUpdate();  
            this.scrollToBottom();
        }, to);
    }

    addOptions(options)
    {
    	clearTimeout(this.optionsTimeout);
    	return new Promise((resolve, reject)=>{
    		this.optionsTimeout = setTimeout(()=>{
    			if(options.length === 0){
                    this.setState({options: []});
    			}else{
                    let opts = options.map((option)=>{
                        return <div className="option" onClick={()=>this.handleClick(option)} data-scenario={option.scenario} data-route={option.route}  key={uuid()}>{option.title}</div>;
                    })
                    this.setState({options: opts});
    			}
	    		resolve("done");
    		}, 100);
    	})
    }


    handleClick(option)
    {
        return option.scenario === "navigate" ? store.dispatch(actionCreators.navigate(option.route)) : this.applyScenario("surprise", option.route);
    }

    scrollToBottom()
    {
    	let element = ReactDOM.findDOMNode(this.chat);
		let to = element.scrollHeight;
        let duration = 800;

        var start = element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 20;

        Math.easeInOutQuad = function (t, b, c, d) {
            t /= d/2;
            if (t < 1) return c/2*t*t + b;
            t--;
            return -c/2 * (t*(t-2) - 1) + b;
        };
        
        var animateScroll = function(){        
            currentTime += increment;
            var val = Math.easeInOutQuad(currentTime, start, change, duration);
            element.scrollTop = val;
            if(currentTime < duration) {
                setTimeout(animateScroll, increment);
            }
        };

        animateScroll();

    }

	render(){
        let optionsStyle = {
            width: this.state.options.length * 130 + "px"
        };
		return <div id="chat" ref={(el)=> {this.chat = el; }}>
					<div className="chatContainer">
                        <GreetingBlock active={this.state.greetingActive} />
						{this.state.messages}
                        <div className="optionsWrap">
                            <div className="options" style={optionsStyle}>
                                {this.state.options}
                            </div>
                        </div>
					</div>
					
				</div>;
	}
};
