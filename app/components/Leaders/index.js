import React from "react";
import MakeRequest from "../../utils/MakeRequest";
import {SERVER_URL, TOKEN} from "../../utils/config";

export default class Tiles extends React.Component{
	constructor(props)
	{
		super(props);
		this.state = {}		
	}

	componentDidMount()
	{
		this.getLeadersFromServer()
			.then(response=>console.log(response))
			.catch(err=>console.log(err))
	}

	getLeadersFromServer()
	{
		return new Promise((resolve, reject)=>{
			MakeRequest({
	            method: 'GET',
	            url: `${SERVER_URL}/rest/api/collections/get/leaders?token=${TOKEN}`,
	            headers: {
	                "Accept": "application/json"
	            }
	        })
	        .then(function(response){                
	            return resolve(JSON.parse(response));
	        })
	        .catch(function (err) {
	            return reject(err);
	        });
		})
	}

	render()
	{
		return <div className="leaders">
			Leadership Team
		</div>
	}
}