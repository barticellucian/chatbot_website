import React from "react";

export default class Typer extends React.Component{
	constructor(props){
        super(props);
        this.state = {
        	word: "",
        	timeToLoad: 250,
        	delayTO: 400
        }
    }

    componentDidMount()
    {
    	setTimeout(::this.type, this.state.delayTO);
    }

    type()
    {
    	this.props.children.split("").forEach((char, i)=>{
    		((index, ch)=>{
    			setTimeout(()=>{
    				this.state.word += ch;
    				this.forceUpdate();
    			}, index*(this.state.timeToLoad/this.props.children.length))
    		})(i, char)
    	})
    }

	render(){
		return <h1>{this.state.word}</h1>
	}
};
