import React from "react";
import MakeRequest from "../../utils/MakeRequest";
import Message from "./Message";

export default class MessageSurprise extends Message{
	constructor(props)
	{
		super(props);
	}
	componentDidMount()
	{
		this.text = this.showMessageDots();
    	this.forceUpdate();
    	this.getSurpriseGif();
	}
	getSurpriseGif()
    {
        MakeRequest({
          method: 'GET',
          url: "http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC"
        })
        .then((response)=>{
            //parse response
            let img = new Image();
            img.src = JSON.parse(response).data.fixed_height_downsampled_url;
            img.onload = ()=>{
                this.text = <div className="surprise"><img className="gif" src={JSON.parse(response).data.fixed_height_downsampled_url} /></div>;
                this.forceUpdate();
                this.props.onMessageComputed();
            }
        })
        .catch((err)=>{
            console.log(err);
        })
    }
}