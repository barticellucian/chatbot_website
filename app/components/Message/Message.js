import React from "react";
import ReactDOM from "react-dom";
import style from "./style.scss";

export default class Message extends React.Component{
	constructor(props){
        super(props);
        this.state = {
        	waiting: true,
        	speedPerChar : 40,
        	speedPerOption: 500,
            minMessageTO: 700
        }
        this.messageTimeout = null;
    }

    componentWillUnmount()
    {
        clearTimeout(this.messageTimeout);
    }

    componentDidMount()
    {
    	//show [...] loading
    	this.text = this.showMessageDots();
    	this.forceUpdate();
    	this.messageTimeout = setTimeout(()=>{
            this.text = <div className="text" dangerouslySetInnerHTML={{__html: this.props.children}}></div>;
            this.forceUpdate();
            this.props.onMessageComputed();
        }, this.computeTiming(this.props.children))
    }


    showMessageDots(content = false)
    {
    	return <div className="dots"></div>;
    }

    computeTiming(content = "..."){
        let messageTO = this.state.speedPerChar*this.messageLength(content);
        return messageTO < this.state.minMessageTO ? this.state.minMessageTO : messageTO;
    }

    messageLength(content)
    {
        return content.replace(/(<img([^>]+)\>)/g, "").length;
    }

	render(){
		return <div className={`message ${this.props.sender}`} data-firstebee={this.props.isFirstEbee}>{this.text}</div>;
	}
};
