import React from "react";
import store from "../../store/index.js";
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import getEntriesFromServer from "../../utils/getEntriesFromServer";
import uuid from "../../utils/uuid.js";
import Message from "./Message";
import {SERVER_URL} from "../../utils/config";

export default class MessageOptions extends Message{
    componentDidMount()
    {
        this.text = this.showMessageDots();
        this.forceUpdate();
        getEntriesFromServer(this.props.children.optionsCat)
            .then((items)=>{
                this.handleOptions(items);
            })
            .catch((err)=>{
                console.log(err);
            })
    }
    
    handleOptions(items)
    {
        //preload thumbs
        let imgCount = 0;

        let serverPath = ((url)=>{
            let urlarr = url.split("/");
            urlarr.pop();
            return urlarr.join("/")
        })(SERVER_URL);

        let links = items.map((item)=>{
            let bgcode = {};
            if(item.hasOwnProperty("thumb")){
                item.thumb = item.thumb.toString().replace("site:cms/", `${serverPath}/`);
                bgcode = { backgroundImage: `url(${item.thumb})` };
            }
            let url = `/${this.props.children.optionsCat}/${item.title_slug}`;
            return <div className={`inlineOption ${url}`} style={bgcode} key={uuid()} onClick={()=>{store.dispatch(actionCreators.navigate(url, true))}}><span>{item.title}</span></div>
            // return <Link to={`/${this.props.children.optionsCat}/${item.title_slug}`}  key={uuid()}>{item.title}</Link>;
        })
        this.text =<div className="options"><div className="wrap" style={{width: `${items.length*190}px`}} >{links}</div></div>;

        items.forEach((item)=>{
            if(item.hasOwnProperty("thumb")){
                let img = new Image();
                let thumb = item.thumb.toString().replace("site:cms/", `${serverPath}/`);
                img.src = thumb;
                img.onload = ()=>{
                    imgCount++;
                    if(imgCount === items.length){
                        this.reloadComponent();
                    }
                }
            }else{
                imgCount++;
                if(imgCount === items.length){
                    this.reloadComponent();
                }
            }
        })
    }

    reloadComponent()
    {
        this.messageTimeout = setTimeout(()=>{
            this.forceUpdate();
            this.props.onMessageComputed();
        }, this.state.minMessageTO)
    }
}