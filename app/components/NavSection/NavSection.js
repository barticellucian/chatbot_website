import React from "react";
import ReactDOM from "react-dom";
import { Link, browserHistory } from 'react-router';
import getEntriesFromServer from "../../utils/getEntriesFromServer";
import uuid from "../../utils/uuid.js";

import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";

export default class NavSection extends React.Component{
    constructor(props)
    {
        super(props);
        this.state = {
            subnavItems : [],
            activeMenu : false,
            height: 0,
            subheight: 0
        };
        this.toggleMenu = this.toggleMenu.bind(this);
        if(this.props.subnav){
            getEntriesFromServer(this.props.subnav)
                .then((items)=>{
                    this.state.subnavItems = this.parseItems(items);
                    this.state.subheight = this.computeHeight();
                })
        }
    }

    parseItems(items)
    {
        return items.map(({title_slug,title}) => <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate(`/${this.props.subnav}/${title_slug}`, true))}} key={uuid()}>{title}</div> )
    }

    toggleMenu()
    {
        this.state.activeMenu = !this.state.activeMenu;
        this.state.height = this.state.activeMenu ? this.computeHeight() : 0;
        this.forceUpdate();
    }

    computeHeight()
    {
        return this.state.subnavItems.length*50+24;
    }

    render()
    {

        let expandButton = <div className="expand" onClick={this.toggleMenu}><span></span><span></span></div>;
        if(this.state.subnavItems.length === 0) expandButton = "";

        return (
            <div className="navItem" data-activesub={this.state.activeMenu}>
                <div className="heading">
                    <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate(`/${this.props.nav}`))}}>{this.props.children}</div>
                    {expandButton}
                </div>
                <nav className="sub" style={{height: this.state.height+"px"}}>
                    <div className="subWrap" style={{height: this.state.subheight+"px"}}>{this.state.subnavItems}</div>
                </nav>
            </div>
        )
    }
}