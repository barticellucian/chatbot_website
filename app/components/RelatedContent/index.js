import React from "react";
import Options from "../Message/MessageOptions"; 

export default (props)=>{
    let stuff = props.cat === "tools" ? "tools" : "case studies";
	return <div className="meta">
		<div className="relatedMessage">
            <p>
                check out our other {stuff}
            </p>
        </div>
        <div className="related">
            <div className="wrap" style={{width: `${props.related.length*190}px`}}>
                {props.related}
            </div>
        </div>
	</div>
}