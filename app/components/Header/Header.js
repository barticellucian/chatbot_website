import React from "react";
import style from "./style.scss";
import NavSection from "../NavSection/NavSection";
import Navigator from "../Navigator";
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";
import Socials from "../Socials";

export default class Header extends React.Component{
    constructor(props)
    {
        super(props);
        this.state = {
            activeMenu : false
        }
        this.toggleMenu = this.toggleMenu.bind(this);
    }
    toggleMenu()
    {
        store.dispatch(actionCreators.toggleMenu(!this.state.activeMenu));
    }

    componentWillReceiveProps(nextProps)
    {
        this.state.activeMenu = nextProps.active;
        this.forceUpdate();
    }

    render()
    {
        return (
            <header data-active={this.state.activeMenu}>
                <div className="heading">
                    <div className="logo" onClick={()=>{store.dispatch(actionCreators.navigate(`/`))}}></div>
                    <div className="menuToggle" onClick={this.toggleMenu}>
                        <span></span><span></span><span></span>
                    </div>
                </div>
                <nav className="main">
                    <div className="navWrap">
                        <NavSection nav="work" subnav="studies">Work</NavSection>
                        <NavSection nav="about" subnav="tools">About</NavSection>
                        <NavSection nav="contact">Contact</NavSection>
                        <NavSection nav="careers">Careers</NavSection>
                    </div>

                    <Socials></Socials>
                </nav>


                <nav className="main-desktop">
                    <div className="heading-half">
                        <div className="headings">
                            <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/work"))}}>work</div>
                            <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/about"))}}>about</div>
                            <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/contact"))}}>contact</div>
                            <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/careers"))}}>careers</div>
                        </div>  
                    </div>
                    <div className="subs-half">
                        <nav className="submenus">
                            <div className="subm">
                                <ul>
                                    <li>
                                        <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/studies/viread-patient-profiles"))}}>case study #1</div>
                                    </li>
                                    <li>
                                        <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/studies/duac"))}}>case study #2</div>
                                    </li>
                                    <li>
                                        <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/studies/cma-live"))}}>case study #3</div>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div className="subm">
                                <ul>
                                    <li>
                                        <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/tools/approach"))}}>our approach</div>
                                    </li>
                                    <li>
                                        <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/tools/our-tools"))}}>how we work</div>
                                    </li>
                                    <li>
                                        <div className="navlink" onClick={()=>{store.dispatch(actionCreators.navigate("/tools/leadership-team"))}}>leadership team</div>
                                    </li>
                                </ul>
                            </div>
                            <div className="subm">
                                <ul>
                                    <li>
                                        +44 (0) 207 440 5630
                                    </li>
                                    <li>
                                        hello@ebeehealth.com
                                    </li>
                                    <li>
                                        <a href="">find us here</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="subm">
                                <ul>
                                    <li>
                                        <a href="">job #1</a>
                                    </li>
                                    <li>
                                        <a href="">job #2</a>
                                    </li>
                                    <li>
                                        <a href="">job #3</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>

                        <Socials></Socials>
                    </div>
                </nav>
            </header>
        )
    }
}