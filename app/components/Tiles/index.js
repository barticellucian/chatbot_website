import React from "react";
import {SERVER_URL, TOKEN} from "../../utils/config";
import MakeRequest from "../../utils/MakeRequest";
import uuid from "../../utils/uuid";

export default class Tiles extends React.Component{
	constructor(props)
	{
		super(props);
		this.state = {
			tiles: []
		}

		let serverPath = ((url)=>{
            let urlarr = url.split("/");
            urlarr.pop();
            return urlarr.join("/")
        })(SERVER_URL);

		this.getTilesFromServer()
			.then(response=>{
				let tiles = response.map(entry=>{
					let path = entry.path.replace("site:cms/", `${serverPath}/`);
					return <div className="tile" key={uuid()}><div className="child" style={{backgroundImage: `url(${path})`}} /></div>;
				}).filter((tile, index)=>index < 16)

				this.shuffle(tiles);

				this.setState({tiles: tiles});
			})
			.catch(err=>console.log(err))
	}

	shuffle(array) {
		//Fisher-Yates (aka Knuth) Shuffle https://github.com/coolaj86/knuth-shuffle
	  var currentIndex = array.length, temporaryValue, randomIndex;

	  // While there remain elements to shuffle...
	  while (0 !== currentIndex) {

	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;

	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	  }

	  return array;
	}

	getTilesFromServer()
	{
		return new Promise((resolve, reject)=>{
			MakeRequest({
	            method: 'GET',
	            url: `${SERVER_URL}/rest/api/galleries/get/tiles?token=${TOKEN}`,
	            headers: {
	                "Accept": "application/json"
	            }
	        })
	        .then(function(response){                
	            return resolve(JSON.parse(response));
	        })
	        .catch(function (err) {
	            return reject(err);
	        });
		})

	}

	render()
	{
		let sets = [];
		for(let i = 0; i<4; i++){
			let set = this.state.tiles.filter((tile, index)=> index >= i*4 && index < (i+1)*4 );
			sets.push(set);
		}
		
		return <div className={`tiles ${this.props.page}`}>
			<div className="tileRow">{sets[0]}</div>
			<div className="tileRow">{sets[1]}</div>
			<div className="tileRow">{sets[2]}</div>
			<div className="tileRow">{sets[3]}</div>
		</div>;
	}

}