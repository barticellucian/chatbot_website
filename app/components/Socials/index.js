import React from "react";
import ReactDOM from "react-dom";

export default (props)=>{
	return <div className="socialWrap">
	            <div className="socials">
	                <a href="#" target="_blank"><span className="instagram"></span></a>
	                <a href="#" target="_blank"><span className="twitter"></span></a>
	                <a href="#" target="_blank"><span className="linkedin"></span></a>
	            </div>
	            <div className="footDetails">
	                &#9400; 2017 The Hive Group Registered in England & Wales No. 6844490 <br/>
	                Registered office: Haymarket House, 1 Oxendon Street, London SW1Y 4EE?
	            </div>
	        </div>
}