import {MESSAGE, OPTION_LABEL} from '../constants';
import {SERVER_URL, TOKEN} from "../utils/config";
import MakeRequest from "../utils/MakeRequest";
import getEntriesFromServer from "../utils/getEntriesFromServer";

export default class Messages{
  constructor()
  {
    this.messages = {};
    this.contentSensitiveMessages = {};
    this.timeSensitiveMessages = {};
    // this.init();
  }
  init()
  {
    return new Promise((resolve, reject)=>{
      Promise.all([
        this.loadMessages(),
        this.loadContentSensitiveMessages(),
        this.loadTimeSensitiveMessages()
      ]).then(values=> resolve(values))
        .catch(err=> reject(err))
    })
  }

  formatResultRoutes(result)
  {
      result.destination = result.destination === "home" ? "/" : `/${result.destination}`;
      result.origin = result.origin === "home" ? "/" : `/${result.origin}`;
      return result;
  }

  loadMessages()
  {

    return new Promise((resolve, reject)=>{
      getEntriesFromServer("messages")
        .then((response)=>{
          let messages = response
              .map(result=>{
                return this.formatResultRoutes(result);
              })
              
              this.messages = messages;
              resolve(messages);
        })
        .catch((err)=>{
          reject(err);
        })
    })
  }

  loadContentSensitiveMessages()
  {

    return new Promise((resolve, reject)=>{
      getEntriesFromServer("cs-messages")
        .then((response)=>{
          let messages = response
              .map(result=>{
                return this.formatResultRoutes(result);
              })
              
              this.contentSensitiveMessages = messages;
              resolve(messages);
        })
        .catch((err)=>{
          reject(err);
        })
    })
  }

  loadTimeSensitiveMessages()
  {
    return new Promise((resolve, reject)=>{
      getEntriesFromServer("ts-messages")
        .then((response)=>{
            this.timeSensitiveMessages = response;
            resolve(response);
        })
        .catch((err)=>{
          reject(err);
        })
    })
  }


  getMessageSet(scenario, route, clientName, previousPath, visitedPages)
  {

      const getSplitRoute = (route)=>{
        if(route.length === 1)
          return "/";
        let routeArr = route.split("/");
        return ["/",routeArr[1]].join("");
      }


      this.clientName = clientName;
      this.route = route;
      this.visitedPages = visitedPages;
      this.splitRoute = getSplitRoute(this.route);
      this.scenario = scenario;
      // return initial ? this.MessagesForNewRoute() : this.MessagesForNavRoute()
      // this.messagesList = this.getMessagesFromServer();
      this.optionsList = this.getOptionsList();
      this.previousPath = previousPath; 


      switch(this.scenario){
        case "first-land":
          return this.MessagesForNewRoute();
        case "navigate": 
          return this.MessagesForPseudoNav();
        case "surprise":
          return this.SurpriseMessage();  
        default:
          return({
            "messages": [],
            "options": []
          })
      }
  }


    MessagesForNewRoute()
    {

      //not used ftm
      // const messages = ()=>{
      //   if(Object.keys(this.messagesList["first-land"]).indexOf(this.splitRoute) > -1)
      //     return this.messagesList[this.scenario][this.splitRoute].concat(this.messagesList.generic[this.splitRoute]);
      //   return this.messagesList.empty;
      // }
      return {
          // "messages": messages(),
          "options": this.getOptionsPerRoute()
        }
      
    }

    MessagesForPseudoNav()
    {

      let messageFromSpecificRoute = this.messages
        .filter(({destination, origin})=>{
            return (this.splitRoute === destination && this.previousPath === origin);
        })
        

      let messageFromAnyRoute = this.messages
        .filter(({destination, origin})=>{
            return (this.splitRoute === destination && "/any" === origin);
        })


      let allMessages = messageFromSpecificRoute.concat(messageFromAnyRoute);

      let messages  = allMessages
        .map(result=>{
            let messagesSet = result.Set.map(message=>{
              return this.parseMessage(message, result.destination, result.origin);
            })      
            return messagesSet;            
        })
        .reduce((acc, cv, i, arr)=>{
            return acc.concat(cv);
        }, [])


      return {
          "messages": messages,
          "options": this.getOptionsPerRoute()
      }
    }

    parseMessage(message, dest, origin)
    {

      const getSpecialMessage = (text, dest, origin)=>{
        text = text.split("");
        text.splice(text.length-1, 1);
        text.splice(0, 1);
        text = text.join("");
        let func = this[text];
        return func.call(this, dest, origin);
      }

      let text = "";
      let sender = "EBEE";
      if(message.value.indexOf(":") > -1){
        let messagedDetails = message.value.split(":");
        sender = messagedDetails[0].toUpperCase();
        text = messagedDetails[1];
        if(/^{\w+}$/.test(text)){
            text = getSpecialMessage(text, dest, origin);
        }else{
          text = twemoji.parse(text);  
        }
      }else{
        text = twemoji.parse(message.value);
      }

      message = text;
      return {
        "message": message, 
        "sender": MESSAGE.SENDER[sender]
      }
    }

    SurpriseMessage()
    {
      const messagesFromServer = this.messages
        .filter(({destination, origin})=>{
            return ("/surprise" === destination && "/any" === origin);
        })
        .map(result=>{
            let messagesSet = result.Set.map(message=>{
              return this.parseMessage(message, result.destination, result.origin);
            })      
            return messagesSet;            
        })

      let surpriseMessages = 
        [
          {
            "message": {type: "surprise"},
            "sender": MESSAGE.SENDER.USER
          }
        ];

      surpriseMessages = surpriseMessages.concat(messagesFromServer[0]);

      return {
          "messages": surpriseMessages,
          "options": this.getOptionsPerRoute()
        }
    }

    getOptionsPerRoute()
    {
      if(Object.keys(this.optionsList["per-route"]).indexOf(this.splitRoute) > -1){
          return this.optionsList["per-route"][this.splitRoute];
      }
        return this.optionsList.empty;  
        
    }

    getTimeSensitiveGreeting()
    {
      let currentDateTime = new Date();
      let currentHours = currentDateTime.getHours();
      let message = `Hello${this.clientName}! 🌅`;
      if(currentHours >= 0 && currentHours < 12){
        message = `Good morning${this.clientName}!  🌅`;
      }else if(currentHours >= 12 && currentHours < 17){
        message = `Good afternoon${this.clientName}! 🌇`;
      }else if(currentHours >= 17 && currentHours < 24){
        message = `Good evening${this.clientName}! 🌃` ;
      }
      return message;
    }

    getContentSensitiveMessage(targetPage, originPage)
    {

      let contentCategory = "studies";
      if(originPage === "/about")
        contentCategory = "tools";


      let itemsViewed = null;
      let visitedCategory = null;


      if(!this.visitedPages.hasOwnProperty(contentCategory)){
          itemsViewed = "noitem";
      }else{
          if(this.visitedPages[contentCategory].length === 1){
            itemsViewed = "oneitem";
            visitedCategory = this.visitedPages[contentCategory][0];
          }else{
            itemsViewed = "moreitems";
          }
      }

      let message = this.contentSensitiveMessages
        .filter(({destination, origin})=>{
            return (targetPage === destination && originPage === origin);
        })
        .map(set=>{
          return set[itemsViewed];
        })

      return itemsViewed === "oneitem" ? `${message[0]} ${this.visitedPages[contentCategory][0]}`: message[0];
    }

    getInlineOptions(destination, origin)
    {
      let optionsCat = "tools";
      if(destination === "/work") optionsCat = "studies";

      return {type: "options", optionsCat: optionsCat};
    }
    


    getOptionsList()
    {
      return {
            "per-route": {
              "/": [
                new Option("navigate", "/work", OPTION_LABEL.work),
                new Option("navigate", "/about", OPTION_LABEL.about), 
                new Option("navigate", "/contact", OPTION_LABEL.contact),
                new Option("surprise", "/",OPTION_LABEL.surprise)
              ],
              "/about": [
                new Option("navigate", "/work", OPTION_LABEL.work),
                new Option("navigate", "/contact", OPTION_LABEL.contact),
                new Option("surprise", "/",OPTION_LABEL.surprise)
              ],
              "/contact": [
                new Option("navigate", "/work", OPTION_LABEL.work),
                new Option("navigate", "/about", OPTION_LABEL.about),
                new Option("surprise", "/",OPTION_LABEL.surprise)
              ],
              "/work": [
                new Option("navigate", "/about", OPTION_LABEL.about),
                new Option("navigate", "/contact", OPTION_LABEL.contact),
                new Option("surprise", "/",OPTION_LABEL.surprise)
              ],
              "/studies": [
                new Option("navigate", "/about", OPTION_LABEL.about),
                new Option("navigate", "/contact", OPTION_LABEL.contact)
              ],
              "/tools": [
                new Option("navigate", "/work", OPTION_LABEL.work),
                new Option("navigate", "/contact", OPTION_LABEL.contact)
              ]
            },
            "empty": []
        }
    }

};


// OptionsFactory
class Option{
  constructor(scenario, route = "", title)
  {
    return {"scenario": scenario, "title": title, "route": route }; 
  }
}