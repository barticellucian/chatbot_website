import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import {browserHistory } from "react-router";
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'; // allows us to use asynchronous actions
import rootReducer from "../reducers";
import { batchedSubscribe } from 'redux-batched-subscribe';

const middleware = routerMiddleware(browserHistory);

const createStoreWithMiddleware = compose(applyMiddleware(middleware, thunk), window.devToolsExtension ? window.devToolsExtension() : f => f)(createStore);

const createStoreWithBatching = batchedSubscribe(
  fn => fn()
)(createStoreWithMiddleware);

const store = createStoreWithBatching(rootReducer);

module.exports = store;